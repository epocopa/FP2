/*
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
// AUTORÍA: DANIEL FIDALGO PANERA

#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>

using namespace std;

const int MAX_PROD = 25;
const int PROD_NULO = 0;
const int CENTINELA = -1;
typedef int tArray[MAX_PROD];

// Imprime el menú
int menu();
// Pide	al usuario el nombre del	fichero	del	que	cargará	su	fila.
void ejecutarLeerFichero(tArray  fila,  int  &tam);
// Guarda	la fila en el fichero designado por el usuiario.
void ejecutarGuardarFichero(const  tArray  fila,  int  tam);
// Pregunta	alusuario	qué	posición	quiere  levantar	y	en	qué
//posición	quiere	dejarlo	caer y realiza el movimiento.
void ejecutarGrua(tArray fila, int tam, int &cont);
// Pregunta	al	usuario	 desde	 qué	 posición	 empujará
// la	 excavadora y realiza el movimiento.
void ejecutarExcavadora(tArray fila, int tam, int &cont);
//Dada	 la	 opción	 escogida	por	el	usuario	en	el	menú,	la	ejecuta.
void  ejecutarOpc(int  opc,  tArray  fila,  int  &tam, int &cont);
// Dado	 el	fichero	de lectura	abierto	fich,	carga	su	contenido	en	la	fila.
void leerFilaFich(ifstream &fich, tArray fila, int &tam);
// Dado	 el	 fichero	 de	 escritura	 abierto	 fich,	 guarda	 en	 dicho
// fichero el	contenido	de	la	fila.
void escribirFilaFich(ofstream  &nuevoFich,  const  tArray  fila,  int  tam);
// Dada	una	fila,	la	muestra	en	pantalla
void mostrarFila(const tArray fila, int tam);
// Devuelve	si la posición	pos	es	válida en	la	fila	de	tamaño	tam.
bool esPosValida(int tam, int pos);
// Devuelve	si la posición pos	está	vacía.
bool estaVacia(const  tArray  fila,  int  pos);
// Devuelve si el segmento a levantar no tiene su extremo
// derecho antes que el izquierdo y si todas las posiciones involucradas son
// válidas en la fila
bool sonPosicionesPosiblesGrua(int tam, int posIni, int posFin, int posSoltar);
// Devuelve	 cierto	 si	 posIni	 y	 posSoltar	 son	 posiciones
// válidas	 de	 la	 fila	 y	 además	 es	 posible	 dejar	 caer	 el
// contenido	 de	 la	 fila	 situado	en	posIni	en	su	posición	posSoltar.
bool esPosibleGrua(const tArray fila, int tam, int posIni, int posFin,
      int posSoltar);
// Si	es	posible,	realiza	el	movimiento	de	la	grúa
//	entre	las	posiciones	posIni y	posSoltar
bool grua(tArray fila, int tam, int posIni, int posFin, int posSoltar);
// Devuelve	la	posición	del	primer	hueco	libre	de	la	fila
int posHuecoLibreDir(const tArray fila, int tam, int posIni, int direccion);
// Desplaza	la	 pala	 de	 la	 excavadora	 desde
// posIni	 una	 posición
bool excavadora(tArray fila, int tam, int posIni, int numDespla, int direccion);
// Devuelve si los materiales en	la fila	están ordenados de menor a mayor
bool filaOrdenada(const tArray fila, int tam, int &menor, int i);


int main() {
  int opc, tam = 0, cont;
  tArray fila;
  while (opc = menu()) {
    ejecutarOpc(opc, fila, tam, cont);
  }
  return 0;
}

int menu(){
  int op;
  do {
    cout << "-------------------------------------------\n";
    cout << " 1 – Cargar fila de fichero\n";
    cout << " 2 – Guarda fila en fichero\n";
    cout << " 3 – Usar grúa\n";
    cout << " 4 – Usar excavadora\n";
    cout << " 0 - Salir\n";
    cout << "-------------------------------------------\n";
    cout << "Introduce una opcción: ";
    cin >> op;
  } while(op < 0 || op > 5);
  return op;
}

void ejecutarLeerFichero(tArray  fila,  int  &tam){
  ifstream fich;
  string nombreFichero;
  cout << "Indique el nombre del fichero: ";
  cin >> nombreFichero;
  fich.open(nombreFichero.c_str());
  if (fich.is_open()) {
    leerFilaFich(fich, fila,tam);
  } else{
    cout << "\nLa lectura del archivo no se ha realizado correctamente\n\n";
  }
}

void ejecutarGuardarFichero(const  tArray  fila,  int  tam){
  string nombreFichero;
  cout << "Indique el nombre del fichero: ";
  cin >> nombreFichero;
  ofstream nuevoFich(nombreFichero.c_str());
  if (nuevoFich.is_open()) {
    escribirFilaFich(nuevoFich, fila, tam);
    cout << "\nLa escritura del archivo se ha realizado correctamente\n\n";
  } else{
    cout << "\nLa escritura del archivo no se ha realizado correctamente\n\n";
  }
}

void ejecutarGrua(tArray fila, int tam, int &cont){
    int posIni, posFin, posSoltar;
    cout << "Indique la posición inicial de los materiales a levantar: ";
    cin >> posIni;
    cout << "Indique la posición final de los materiales a levantar: ";
    cin >> posFin;
    cout << "Indique en que posición quiere dejarlo caer: ";
    cin >> posSoltar;
    if (esPosibleGrua(fila, tam, posIni, posFin, posSoltar)){
        grua(fila, tam, posIni, posFin, posSoltar);
        cont++;
    } else {
    cout << "\nLa operación no se ha realizado correctamente\n\n";
    }
}

void ejecutarExcavadora(tArray fila, int tam, int &cont){
  int direccion, posIni, numDespla;
  cout << "Indique que posición quiere empujar: ";
  cin >> posIni;
  do {
    cout << "Indique la dirección-> -1(Izquierda) 1(Derecha): ";
    cin >> direccion;
  } while(direccion != 1 && direccion !=  -1);
  cout << "Indique el número de desplazamientos que desea realizar: ";
  cin >> numDespla;
  if (excavadora(fila, tam, posIni, numDespla, direccion)){
      cont++;
  } else {
    cout << "\nLa operación no se ha realizado correctamente\n\n";
  }
}

void ejecutarOpc(int  opc,  tArray  fila,  int  &tam, int &cont){
  int menor, i = 0;
  switch (opc) {
    case 1:
      ejecutarLeerFichero(fila, tam);
      mostrarFila(fila, tam);
      cont = 0;
      break;
    case 2:
      ejecutarGuardarFichero(fila, tam);
      mostrarFila(fila, tam);
      break;
    case 3:
      ejecutarGrua(fila, tam, cont);
      mostrarFila(fila, tam);
      if (!filaOrdenada(fila, tam, menor, i)) {
			  cout << "\nLa fila todavía no esta ordenada, llevas "
        << cont << " movimientos\n\n";
		  } else {
			  cout << "\n¡Fila ordenada! Completada en "
        << cont << " movimientos\n\n";
		  }
      break;
    case 4:
      ejecutarExcavadora(fila, tam, cont);
      mostrarFila(fila, tam);
      if (!filaOrdenada(fila, tam, menor, i)) {
			  cout << "\nLa fila todavía no esta ordenada, llevas "
        << cont << " movimientos\n\n";
		  } else {
        cout << "\n¡Fila ordenada! Completada en " <<
        cont << "movimientos\n\n";
		  }
      break;
  }
}

void leerFilaFich(ifstream &fich, tArray fila, int &tam){
  int n, i = 0;
  fich >> n;
  while (n != CENTINELA && i < MAX_PROD ) {
    fila[i] = n;
    i++;
    fich >> n;
  }
  fich.close();
  tam = i;
}

void escribirFilaFich(ofstream  &nuevoFich,  const  tArray  fila,  int  tam) {
    for (int i = 0; i < tam; i++) {
      nuevoFich << fila[i] << " ";
    }
    nuevoFich << "-1";
    nuevoFich.close();
}

void mostrarFila(const tArray fila, int tam){
  cout << "\n";
  for (int i = 0; i < tam; i++) {
    if(fila[i] != 0){
      cout << "|" << setw(2) << fila[i];
    } else {
      cout << "|  ";
    }
  }
  cout << "|\n";
  for (int i = 0; i < tam; i++) {
      cout << "---";
  }
  cout << "-\n";
  for (int i = 0; i < tam; i++) {
      cout << "|" << setw(2) << i;
  }
  cout << "|\n\n";
}

bool esPosValida(int tam, int pos){
  if (pos < tam && pos >= 0) {
    return true;
  } else {
    return false;
  }
}

bool estaVacia(const  tArray  fila,  int  pos){
  if (fila[pos] == 0) {
    return true;
  } else {
    return false;
  }
}

bool sonPosicionesPosiblesGrua(int tam, int posIni, int posFin, int posSoltar){
  bool validasEnTam = false;
  if(esPosValida(tam, posIni) && esPosValida(tam, posFin)
  && esPosValida(tam, posSoltar)
  && esPosValida(tam, posSoltar + (posFin - posIni))){
    validasEnTam = true;
  }
  if (posIni < posFin && validasEnTam) {
    return true;
  } else{
    return false;
  }
}

bool esPosibleGrua(const tArray fila, int tam, int posIni, int posFin,
      int posSoltar){
  tArray aux;
  int c = posIni, x = 0;
  for (int i = 0; i < tam; i++) {
    aux[i] = fila[i];
  }
  for (int j = posIni; j <= posFin; j++) {
      aux[c+x] = 0;
      x++;
  }
  bool vacias = false;
  int max = posFin - posIni+1;
  int j = 0; //debe ser igual a el numero de huecos a comprobar
  for (int i = 0; i < max; i++) {
    if (estaVacia(aux, posSoltar+i)) {
      j++;
    } else if (fila[posIni+i] == 0) {
      j++;
    }
  }
  if (j == max) {
    vacias = true;
  }
  if (sonPosicionesPosiblesGrua(tam, posIni, posFin, posSoltar) && vacias) {
      return true;
  } else {
      return false;
  }
}

bool grua(tArray fila, int tam, int posIni, int posFin, int posSoltar){
  tArray aux;
  int j = posIni;
  int max = posSoltar + (posFin - posIni);
  for (int i = posSoltar; i <= max; i++) { //copiar levantados a aux
    aux[i] = fila[j];
    fila[j] = 0;
    j++;
  }
  for (int n = posSoltar; n <= max; n++) { //devolver levantados a fila
    if (aux[n] != 0) {
      fila[n] = aux[n];
      aux[n] = 0;
    }
  }
    return true;
}

int posHuecoLibreDir(const tArray fila, int tam, int posIni, int direccion){
    int i = posIni, x;
    for (i; fila[i] != 0; i+=direccion) {
      x = i+direccion;
    }
    if (x >= tam && x > 0){
        return tam;
    } else{
        return x;
    }
}

bool excavadora(tArray fila, int tam, int posIni, int numDespla, int direccion){
  int c = 0, x;
  if (direccion == 1) {
    x = -1;
  } else{
    x = 1;
  }
  for (int j = 0; j < numDespla; j++) {
    int i = posHuecoLibreDir(fila, tam, posIni+(j * direccion), direccion);
    if (i < tam && i >= 0) {
      for(i; i != posIni;i+=x){
        fila[i] = fila[i+x];
        fila[i+x] = 0;
      }
      c++;
    }
  }
  if (c == numDespla) {
    return true;
  } else {
      return false;
    }
}

bool filaOrdenada(const tArray fila, int tam, int &menor, int i) {
	bool ok = true;
		while (fila[i] == PROD_NULO && i+1 <= tam) {
			i++;
		}
		menor = fila[i];
		while (fila[i+1] == PROD_NULO && i+1 <= tam) {
			i++;
		}
		while (i+1 < tam && (fila[i+1] != PROD_NULO) && (fila[i+1] != -1) && ok) {
			if (menor < fila[i + 1]) {
				return filaOrdenada(fila, tam, menor, i + 1);
			}
			else {
				ok = false;
			}
		}
	return ok;
}
/*
«No te preocupes si no funciona bien. Si todo lo hiciera, no tendrías trabajo.»

-La ley de Mosher sobre la ingeniería de Software
*/
